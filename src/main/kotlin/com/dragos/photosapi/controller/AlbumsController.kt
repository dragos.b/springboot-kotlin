package com.dragos.photosapi.controller

import com.dragos.photosapi.model.Album
import com.dragos.photosapi.service.AlbumService
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.responses.ApiResponse
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

// Controller for API Endpoint
@RestController
@RequestMapping("/api/albums", "/api/albums/")
class AlbumsController(
    // DI
    @Autowired private val albumService: AlbumService
) {

    /* Configuration for OpenAPI */
    @Operation(summary = "Get all albums in array list")

    /* Configuration for Spring */
    @GetMapping("/")
    fun getAllAlbums(): ResponseEntity<*> {
        val body: List<Album> = albumService.getAlbums()

        val returnedValue: ResponseEntity<*> = if (body.isNotEmpty()) {
            ResponseEntity
                .status(HttpStatus.OK)
                .body(body)

        } else {
            ResponseEntity
                .status(HttpStatus.NO_CONTENT)
                .body("No Data")
        }
        return returnedValue
    }


    /* Configuration for OpenAPI */
    @Operation(summary = "Get one album by ID")
    @ApiResponse(
        responseCode = "200",
        description = "Get album by id",
        content = [
            Content(mediaType = "application/json")
        ]
    )
    @ApiResponse(
        responseCode = "204",
        description = "No photos in the album",
    )

    /* Configuration for Spring */
    @GetMapping("/{albumId}", "/{albumId}/")
    fun getAlbumById(@PathVariable albumId: Int): ResponseEntity<*> {
        val body = albumService.getAlbumById(albumId)

        val responseValue = if (body !== null) {
            // If body has data return data to the client
            ResponseEntity
                .status(HttpStatus.OK)
                .body(body)
        } else {
            // If body is empty return 204 status code
            ResponseEntity
                .status(HttpStatus.NO_CONTENT)
                .build()
        }
        return responseValue
    }

    /* Configuration for OpenAPI */
    @Operation(summary = "Get all photos from album with ID")
    @ApiResponse(
        responseCode = "200",
        description = "All photos in an album by id",
        content = [
            Content(mediaType = "application/json")
        ]
    )
    @ApiResponse(
        responseCode = "204",
        description = "No photos in the album",
    )

    /* Configuration for Spring */
    @GetMapping("/{albumId}/photos", "/{albumId}/photos/")
    fun getPhotosByAlbumId(@PathVariable albumId: Int): ResponseEntity<*> {
        val body = albumService.getPhotos(albumId)

        val responseValue = if (body.isNotEmpty()) {
            ResponseEntity
                .status(HttpStatus.OK)
                .body(body)
        } else {
            ResponseEntity
                .status(HttpStatus.NO_CONTENT)
                .build()
        }
        return responseValue
    }

}