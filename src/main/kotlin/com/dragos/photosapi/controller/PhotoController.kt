package com.dragos.photosapi.controller

import com.dragos.photosapi.service.PhotoService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController


@RestController
@RequestMapping("/api/photos", "/api/photos/")
class PhotoController(@Autowired private val photoService: PhotoService) {

    @GetMapping("/{photoId}", "/{photoId}/")
    fun getPhotoById(@PathVariable photoId: Int): ResponseEntity<*> {
        val body = photoService.getPhotoById(photoId)
        val returnedValue = if (body !== null) {
            ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(body)
        } else {
            ResponseEntity
                .status(HttpStatus.NO_CONTENT)
                .build()
        }
        return returnedValue

    }
}