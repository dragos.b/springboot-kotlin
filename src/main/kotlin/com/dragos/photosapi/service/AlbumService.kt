package com.dragos.photosapi.service

import com.dragos.photosapi.model.Album
import com.dragos.photosapi.model.Photo
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate


@Service
class AlbumService(
    /* DI */
    @Autowired private val restTemplate: RestTemplate,
    @Value("\${external.api.url}") private val externalApiUrl: String
) {

    private val logger = LoggerFactory.getLogger(AlbumService::class.java)
    private val ALBUMS_PATH = "albums"
    val URI = "$externalApiUrl/$ALBUMS_PATH"


    // Fetch all albums from external API and convert to List of Album data class.
    fun getAlbums(): List<Album> {
        return try {
            val response = restTemplate.getForEntity(URI, Array<Album>::class.java)
            response.body?.toList() ?: emptyList()
        } catch (e: Exception) {
            logger.error("Error fetching albums: ${e.message}")
            emptyList()
        }
    }

    // Fetch one album with id
    fun getAlbumById(albumId: Int): Album? {
        return try {
            restTemplate.getForObject("$URI/$albumId", Album::class.java)
        } catch (e: Exception) {
            logger.error("Error fetching album by id $albumId: ${e.message}")
            null
        }
    }

    // Fetch all photos from specific album
    fun getPhotos(albumId: Int): List<Photo> {
        return try {
            val url = "$URI/$albumId/photos"
            val response = restTemplate.getForEntity(url, Array<Photo>::class.java)
            response.body?.toList() ?: emptyList()
        } catch (e: Exception) {
            logger.error("Error fetching photos for album $albumId: ${e.message}")
            emptyList()
        }
    }
}