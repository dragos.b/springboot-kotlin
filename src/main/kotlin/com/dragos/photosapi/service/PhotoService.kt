package com.dragos.photosapi.service

import com.dragos.photosapi.model.Photo
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate

@Service
class PhotoService(
    /* DI */
    @Autowired private val restTemplate: RestTemplate,
    @Value("\${external.api.url}") private val externalApiUrl: String
) {
    private val PHOTOS_PATH = "photos"
    private val URI = "$externalApiUrl/$PHOTOS_PATH"

    // Fetch one photo with ID
    fun getPhotoById(photoId: Int): Photo? {
        return try {
            restTemplate.getForObject("$URI/$photoId", Photo::class.java)
        } catch (e: Exception) {
            null
        }
    }
}