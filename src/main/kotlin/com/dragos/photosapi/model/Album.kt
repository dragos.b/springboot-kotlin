package com.dragos.photosapi.model

data class Album(
    val userId: Int,
    val id: Int,
    val title: String
)
