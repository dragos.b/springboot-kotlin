package com.dragos.photosapi

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class PhotosapiApplication {

}

fun main(args: Array<String>) {
	runApplication<PhotosapiApplication>(*args)
}
