package com.dragos.photosapi.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.client.RestTemplate

@Configuration
class AppConfig {

    // Provision RestTemplate object to dependency injector framework
    @Bean
    fun restTemplate(): RestTemplate {
        return RestTemplate()
    }
}