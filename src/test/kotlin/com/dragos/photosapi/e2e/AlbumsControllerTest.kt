package com.dragos.photosapi.e2e

import com.dragos.photosapi.service.AlbumService
import org.hamcrest.Matchers.*
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@SpringBootTest
@AutoConfigureMockMvc
class AlbumsControllerTest {

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Test
    fun `getAllAlbums returns a list of albums`() {
        mockMvc.perform(
            get("/api/albums/")
                .contentType(MediaType.APPLICATION_JSON)
        )
            .andExpect(status().isOk)
            .andExpect(jsonPath("$", hasSize<Any>(greaterThan(0))))
    }

    @Test
    fun `getAlbumById returns an album`() {
        mockMvc.perform(
            get("/api/albums/1")
                .contentType(MediaType.APPLICATION_JSON)
        )
            .andExpect(status().isOk)
            .andExpect(jsonPath("$.id", `is`(1)))
    }

    @Test
    fun `getPhotosByAlbumId returns a list of photos`() {
        mockMvc.perform(
            get("/api/albums/1/photos")
                .contentType(MediaType.APPLICATION_JSON)
        )
            .andExpect(status().isOk)
            .andExpect(jsonPath("$", hasSize<Any>(greaterThan(0))))
    }


    @Test
    fun `getAlbumById incorrect ID`() {
        mockMvc.perform(
            get("/api/albums/1000")
                .contentType(MediaType.APPLICATION_JSON)
        )
            .andExpect(status().isNoContent)
    }

}