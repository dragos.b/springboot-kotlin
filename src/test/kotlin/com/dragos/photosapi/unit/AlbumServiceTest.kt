package com.dragos.photosapi.unit

import com.dragos.photosapi.model.Album
import com.dragos.photosapi.model.Photo
import com.dragos.photosapi.service.AlbumService
import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.jupiter.api.Test
import org.mockito.BDDMockito.given
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@SpringBootTest
@AutoConfigureMockMvc
class AlbumServiceTest {
    @Autowired
    private lateinit var mockMvc: MockMvc

    @MockBean
    private lateinit var albumService: AlbumService

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    @Test
    fun `getAllAlbums returns a list of albums`() {
        // Define a fixed response for albumService.getAlbums()

        val albums = listOf(
            Album(10, 1, "Album 1"),
            Album(20, 2, "Album 2")
        )

        given(albumService.getAlbums()).willReturn(albums)

        // Perform a GET request to /api/albums/
        mockMvc.perform(
            get("/api/albums/")
                .contentType(MediaType.APPLICATION_JSON)
        )
            .andExpect(status().isOk)
            .andExpect(content().json(objectMapper.writeValueAsString(albums)))
    }

    @Test
    fun `getAlbumById returns an album`() {
        // Define a fixed response for albumService.getAlbumById()
        val album = Album(10, 1, "Album 1")

        given(albumService.getAlbumById(1)).willReturn(album)

        // Perform a GET request to /api/albums/1
        mockMvc.perform(
            get("/api/albums/1")
                .contentType(MediaType.APPLICATION_JSON)
        )
            .andExpect(status().isOk)
            .andExpect(content().json(objectMapper.writeValueAsString(album)))
    }

    @Test
    fun `getPhotosByAlbumId returns a list of photos`() {
        // Define a fixed response for albumService.getPhotos()
        val photos = listOf(
            Photo(1, 1, "Photo 1", "url", "thumb url"),
            Photo(1, 2, "Photo 1", "url", "thumb url")
        )

        given(albumService.getPhotos(1)).willReturn(photos)

        // Perform a GET request to /api/albums/1/photos
        mockMvc.perform(
            get("/api/albums/1/photos")
                .contentType(MediaType.APPLICATION_JSON)
        )
            .andExpect(status().isOk)
            .andExpect(content().json(objectMapper.writeValueAsString(photos)))
    }

}