# Spring Boot & Kotlin

Implementacion básica de un API en Spring Boot sobre Kotlin y usando OpenAPI para documentar la API.

## Resumen

La aplicación se encarga de realizar peticiones a una API externa
concretamente a `jsonplaceholder` y a continuacion retorna los datos al cliente.

Consta de dos endpoints

- `albums`
- `photo`

### Albums

El endpoint de `albums` retorna datos en formato JSON, los datos
retornados son los siguientes:

- Una lista de Albums
- Un album en concreto

### Photos

El endpoint de `photos` está pensado solo para retornar una unica foto mediante un id de foto.
Se ha realizado de esta forma ya que las fotos las vemos en la lista de albumes o en un album en
concreto.

## Arquitectura

La arquitectura del proyecto sigue los principios de Clean Architecture
por lo que la organización de los directorios es la siguiente

| Directorio   | Capa de Clean Architecutre                 |
|--------------|--------------------------------------------|
| `controller` | Capa intermedia (Interface Adapters)       |
| `service`    | Capa interior (Application Business Rules) |
| `model`      | Capa interior (Enterprise Business Rules)  |

> Para más información sobre Clean Architecture comparto enlace a un post que lo explico:
> [post en dev.to](https://dev.to/dragosb/arquitectura-limpia-2e6j)

También se hace uso de la inyección de dependecias para conseguir una separación de dependencias
y así realizar testing de manera sencilla.

## Requisitos

- `JDK 17`
- `Kotlin 1.8.22`
- `Spring Boot 3.1.5`

## Ejecución

La documentación de la API se genera automáticamente usando Springdoc OpenAPI y está disponible en la ruta
`/swagger-ui.html` una vez que la aplicación esté en ejecución.

## Testing

En la parte de las pruebas se ha realizado dos tipos de testing, de integración y de unidad.
Los tests de integración simulan a un cliente que accede a la API y los test unitarios prueban los controladores.

### Tests e2e

Los tests end-to-end los he guardado en `src/test/kotlin/com/dragos/photosapi/e2e` y lo que he testeado
es el controlador de Albumes.

Los siguientes pasos serían:

- Añadir más tests, probando todos los casos.
- Añadir tests para el controlador de `photos`

### Tests Unitarios

Los tests unitarios los he guardado en `src/test/kotlin/com/dragos/photosapi/unit/` y se encargan de
probar el `AlbumService`

## Proximos avances

Las proximas tareas son:

- [ ] Documentar OpenAPI de forma más extensa
- [ ] Cambiar el error 404
- [ ] Crear un manejador de errores personalizado


